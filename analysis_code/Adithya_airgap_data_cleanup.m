clear all
close all

root_path = '\\dm11\turnerlab\Mehrab\Adithya_airgap_MB630B\';

cd(root_path)

all_folders1 = dir;
all_folders1(1:2) = [];

for dir1_n = 1:length(all_folders1)
    curr_dir1 = all_folders1(dir1_n).name;
    cd([root_path, curr_dir1])
    
    all_folders2 = dir;
    all_folders2(1:2) = [];
    
    for dir2_n = 1:length(all_folders2)
        curr_dir2 = all_folders2(dir2_n).name;
        curr_path = [root_path, curr_dir1, '\', curr_dir2, '\'];
        cd(curr_path)
        
        movie_fnames = dir('*.ufmf');
        
        %deleting movie file copies
        delete *.ufmf
            
    end
  
    
end
